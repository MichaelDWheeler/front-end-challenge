import React from 'react';
import * as DomainActions from '../actions/DomainActions.jsx';

class Button extends React.Component{
    constructor(){
        super();
    }

    _saveDomainInfo(){
        DomainActions.switchComponents();
    }

    render(){
        return(
            <div className="col-xs-8 col-xs-offset-4 col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2">
                <button onClick={this._saveDomainInfo.bind(this)} className="button">{this.props.text}</button>
            </div>
        )
    }
}

module.exports = Button;
