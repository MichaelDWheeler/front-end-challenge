import React from 'react';

class Heading extends React.Component{
    render(){
        return(
            <div className="row">
                <div className="heading">
                    <div className="col-xs-6 col-sm-8">
                        Domain name
                    </div>
                    <div className="col-xs-4 col-sm-3 centered">
                        Uniregistry
                    </div>
                    <div className="col-xs-2 col-sm-1">
                        <span className="flow-right">Price</span>
                    </div>
                </div>
            </div>
        )
    }
}

module.exports = Heading;
