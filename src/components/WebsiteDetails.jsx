import React from 'react';
import Button from '../components/Button.jsx';

class WebsiteDetails extends React.Component{
    render(){
        return(
            <div className="domain-name-container">
                <div className="row">
                    <div class="website-details">
                        <div className="row p-b-5">
                            <div className="col-xs-4 col-sm-3 col-md-2">
                                Domain name
                            </div>
                            <div className="col-xs-8 col-sm-9 col-md-10">
                                <input type="text" value={this.props.value} readOnly />
                            </div>
                        </div>
                        <div className="row p-t-5 p-b-5">
                            <div className="col-xs-4 col-sm-3 col-md-2">
                                Registrant Email
                            </div>
                            <div className="col-xs-8 col-sm-9 col-md-10">
                                <input type="text" value={this.props.email} readOnly />
                            </div>
                        </div>
                        <div className="row p-t-5 p-b-5">
                            <div className="col-xs-4 col-sm-3 col-md-2">
                                Price
                            </div>
                            <div className="col-xs-8 col-sm-9 col-md-10">
                                <input type="text" value={this.props.price} readOnly />
                            </div>
                        </div>
                        <div className="row p-t-5">
                            <Button text="Save changes"/>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

module.exports = WebsiteDetails;
