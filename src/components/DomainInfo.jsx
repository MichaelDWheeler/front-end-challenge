import React from 'react';
import * as DomainActions from '../actions/DomainActions.jsx';
import DomainStore from '../stores/DomainStore.jsx';
import jQuery from 'jquery';

class DomainInfo extends React.Component{
    constructor(props){
        super(props);
    }

    _checkDomainName(website){
        let extension = website.split(".");
        if(extension[1] === "lol" || extension[1] === "cars"){
            return (
                <div>&#10004;</div>
            )
        }
    }

    _setBackground(id){
        if(id % 2 === 0){
            return true;
        }
    }

    _showDomainInfo(){
        DomainActions.switchComponents();
        let id = this.props.id;
        DomainStore._getWebsiteDetails(id);
    }

    render(){
        let websiteStyle = {
            background: '#f9f9f9d'
        };
        if(this._setBackground(this.props.id)){
            websiteStyle['background'] = "#ffffff";
        }
        return(
            <div className="row background" style={websiteStyle}>
                <div class="website">
                    <div className="col-xs-6 col-sm-8 green" onClick={this._showDomainInfo.bind(this)}>
                         {this.props.domain}
                    </div>
                    <div className="col-xs-4 col-sm-3 green centered">
                        {this._checkDomainName(this.props.domain)}
                    </div>
                    <div className="col-xs-2 col-sm-1">
                        <span className="flow-right">${this.props.price}</span>
                    </div>
                </div>
            </div>
        )
    }
}

module.exports = DomainInfo
