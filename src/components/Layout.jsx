import React from 'react';
import Heading from '../components/Heading.jsx';
import WebsiteDetails from '../components/WebsiteDetails.jsx';
import DomainInfo from '../components/DomainInfo.jsx';
import * as DomainActions from '../actions/DomainActions.jsx';
import DomainStore from "../stores/DomainStore.jsx";

class Layout extends React.Component {
    constructor(props){
        super();
        this.state ={
            "domains": [],
            "websiteInfo": [{
                "showDetails": false
            }],
            "websiteDetails" :[]
        };
    }

    componentWillMount(){
        DomainStore._getJsonData();
        DomainStore.on("switchComponents", () => {
            this.setState(DomainStore._getAll());
        });
    }

    render(){
        let detailedInfo, websites;
        if(this.state.websiteInfo[0].showDetails) {
            detailedInfo = <WebsiteDetails value={this.state.websiteDetails.domain} email={this.state.websiteDetails.registrant_email} price={this.state.websiteDetails.price}/>
        }else{
            websites = <div className="domain-name-container">
                <Heading />
                {this.state.domains.map((domain) => (
                    <DomainInfo key={domain.id} domain={domain.domain} price={domain.price} id={domain.id}/>
                ))}
            </div>
        }
        return(
            <div>
                <div className="container">
                    {websites}
                    {detailedInfo}
                </div>
            </div>

    )
}
}

module.exports = Layout;
