import dispatcher from '../dispatcher.jsx';

export function getData(){
    dispatcher.dispatch({
        type: "GET_DATA"
    })
}

export function switchComponents(){
    dispatcher.dispatch({
        type: "SWITCH_COMPONENT"
    })
}
