import { EventEmitter } from 'events';
import dispatcher from '../dispatcher.jsx';
import jQuery from 'jquery';

class DomainStore extends EventEmitter {
    constructor(){
        super();
        this.domain = {
            "domains": [],
            "websiteInfo": [{
                "showDetails": false
            }],
            "websiteDetails" :{}
        }
    }

    _getJsonData(){
        jQuery.ajax({
            method: 'GET',
            url: '../domains.json',
            success: (domains)=>{
                this.domain.domains = domains.domains;
                this.emit('switchComponents');
            }
        })
    }

    _getWebsiteDetails(id){
        jQuery.ajax({
            method: 'GET',
            url: id +'.json',
            success: (websiteDetails) => {
                this.domain.websiteDetails = websiteDetails;
                this.emit('switchComponents');
            }
        })
    }
    _getAll(){
        return this.domain;
    }


    _switchComponents(){
        if(this.domain.websiteInfo[0].showDetails === false){
            this.domain.websiteInfo[0].showDetails = true;
        }else{
            this.domain.websiteInfo[0].showDetails = false;
            this.domain.websiteDetails.domain = '';
            this.domain.websiteDetails.registrant_email = '';
            this.domain.websiteDetails.price = '';

        }
        this.emit('switchComponents');
    }

    handleActions(action){
        switch(action.type){
            case "GET_DATA": {
                _getData();
            }
            break;
            case "SWITCH_COMPONENT": {
                this._switchComponents();
            }
            break;
        }

    }
}

const domainStore = new DomainStore;
dispatcher.register(domainStore.handleActions.bind(domainStore));

export default domainStore;
