var express = require('express');
var app = express();

app.use(express.static('src'));

app.listen(process.env.PORT || 1337);
console.log('Port 1337 is listening');
